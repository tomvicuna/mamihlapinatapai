import json

file = open('mapudungun-a.txt', 'r')

dictionary = []

for line in file:
    print line
    word_split = line.split('-')
    word = word_split[1]
    definition = word_split[0]

    dictionary.append({
        'word': word,
        'definition': definition
    })

with open('data.json', 'w') as outfile:
    json.dump(dictionary, outfile)

