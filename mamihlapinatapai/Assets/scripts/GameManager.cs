﻿using UnityEngine;
using UnityEngine.UI;

using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Mamihlapinatapai
{
	public enum GameLanguage
	{
		Mapudungun,
		Aymara,
		Rapanui,
	}

	[Serializable]
	public class Definition{
		public Text word;
		public Text definition;
		public Text guess;
	}

	public class GameManager : MonoBehaviour
	{

		private bool _canPlay = true;
			
		public Definition first;
		public Definition second;
		public Definition third;
		public Definition fourth;

		private List<WordDefinition> _dictionary;
		private DictionaryLoader _dictionaryLoader = new DictionaryLoader ();

		private GameState _state;
		private StateGenerator _stateGenerator = new StateGenerator ();

		private MainViewModel _viewModel = new MainViewModel();

		public GameLanguage gameLanguage;

		public Text questionText;

		public Dropdown dropdown;

		public Image background;
		public Image[] boxes;


		[SerializeField] ColorUtility colorUtility;
		ScoreManager scoreManager;
		AudioManager audioManager;

		void Start ()
		{
			dropdown.onValueChanged.AddListener(delegate {
				DropdownValueChangedHandler(dropdown);
			});

			dropdown.options.Clear ();
			var languages = Enum.GetValues(typeof(GameLanguage));
			dropdown.captionText.text = languages.GetValue (0).ToString ();
			foreach (var language in languages)
				dropdown.options.Add (new Dropdown.OptionData() { text=language.ToString() });
			dropdown.value = (int)gameLanguage;
			audioManager = GetComponent<AudioManager>();
			scoreManager = GetComponent<ScoreManager>();

			_dictionary = _dictionaryLoader.Load (gameLanguage);
			_state = _stateGenerator.Generate (_dictionary);
			scoreManager.Init();
			InitializeViewModel ();
			Screen.orientation = ScreenOrientation.Portrait;
		}

		private void DropdownValueChangedHandler(Dropdown target) {
			gameLanguage = (GameLanguage)target.value;
			_dictionary = _dictionaryLoader.Load (gameLanguage);
			_viewModel.ColorPallete = colorUtility.GetPallete (gameLanguage);
			RefreshStates ();
		}

		void Destroy() {
			dropdown.onValueChanged.RemoveAllListeners();
		}

		private void InitializeViewModel()
		{
			_viewModel.Question = questionText;

			_viewModel.AddWord(new WordDefinitionViewModel(first));
			_viewModel.AddWord(new WordDefinitionViewModel(second));
			_viewModel.AddWord(new WordDefinitionViewModel(third));
			_viewModel.AddWord(new WordDefinitionViewModel(fourth));

			_viewModel.Background = background;
			_viewModel.Boxes = boxes;

			_viewModel.ColorPallete = colorUtility.GetPallete (gameLanguage);

			_viewModel.State = _state;
		}

		public void RefreshStates ()
		{
			_state = _stateGenerator.Generate (_dictionary);
			_viewModel.State = _state;
		}

		public void GuessWord (int index)
		{
			
			if (!_state.Done  )
			{

				if (_state.CurrentWordIndex == index)
				{
					NotifyRightGuess (index);
				}
				else
				{
					AlertWrongGuess ();
				}

				if(_state.Done){
					questionText.text = "MUY BIEN!";
					audioManager.PlayCompleteLevel();
					scoreManager.AddScore(3);
					Invoke("RefreshStates", 1f);

				}

			}
		}

		private void NotifyRightGuess(int index)
		{
			_viewModel.WordGuessed (index);
			_state.AdvanceWord ();
			scoreManager.AddScore();
			audioManager.PlayRightGuessAudio();
			StartCoroutine (RightAnswerColorEffect ());
		}

		private void AlertWrongGuess ()
		{
			scoreManager.SubstractScore();
			audioManager.PlayWrongGuessAudio();
			StartCoroutine (WrongAnswerColorEffect ());

		}

		IEnumerator WrongAnswerColorEffect (float waitTime = 0.1f)
		{
			_viewModel.SetBackgroundWrongColor ();
			yield return new WaitForSeconds (waitTime);
			_viewModel.SetBackgroundDefaultColor ();
			_canPlay = true;
		}

		IEnumerator RightAnswerColorEffect (float waitTime = 0.1f)
		{
			_viewModel.SetBackgroundRightColor ();
			yield return new WaitForSeconds (waitTime);
			_viewModel.SetBackgroundDefaultColor ();
			_canPlay = true;
		}

		// Update is called once per frame
		void Update ()
		{
			if (Input.GetKeyDown (KeyCode.Alpha1)) GuessWord (0);
			else if (Input.GetKeyDown (KeyCode.Alpha2)) GuessWord (1);
			else if (Input.GetKeyDown (KeyCode.Alpha3)) GuessWord (2);
			else if (Input.GetKeyDown (KeyCode.Alpha4)) GuessWord (3);
		}
	}
}




