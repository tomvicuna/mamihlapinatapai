﻿using UnityEngine;
using System.Collections;

public class BackgroundManager : MonoBehaviour {
	[SerializeField] GameObject rowPrefab;
	float separation = 0.75f;
	float speed = .02f;
	Vector3 initialPos;
	// Use this for initialization
	void Start () {
		float size = Camera.main.orthographicSize;
		initialPos = Vector3.zero;
		initialPos.y = size;
		while(initialPos.y > -size){
			GameObject instance = Instantiate(rowPrefab, initialPos, transform.rotation) as GameObject;
			instance.GetComponent<Scrolluv>().SetSpeed(speed);
			instance.transform.SetParent(this.gameObject.transform);
			speed *= -1;
			initialPos.y -= separation;
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
