﻿using UnityEngine;
using System.Collections;

public class Scrolluv : MonoBehaviour {
	float _speed;
	// Use this for initialization
	void Update () {
		MeshRenderer mr = GetComponent<MeshRenderer>();
		Material m = mr.material;

		Vector2 offset = m.mainTextureOffset;
		offset.x += _speed*Time.deltaTime;
		m.mainTextureOffset = offset;
	}

	public void SetSpeed(float speed){
		_speed = speed;
	}
	

}
