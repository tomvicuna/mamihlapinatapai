﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace Mamihlapinatapai
{
	public class GameState
	{
		private const int numberOfWords = 4;
		private WordDefinition doneMessage = new WordDefinition ("", "");

		private WordDefinition _currentWord;
		private List<WordDefinition> _words;

		private int _index = 0;
		private int[] _wordOrder;
		private bool _done = false;

		public WordDefinition CurrentWord
		{
			get { return _words[CurrentWordIndex]; }
		}

		public bool Done
		{
			get { return _done; }
		}

		public WordDefinition NextWord
		{
			get
			{ 
				if (_index < numberOfWords - 1)
					return _words [NextWordIndex];
				else
					return doneMessage;
			}
		}
			
		public int CurrentWordIndex
		{
			get { return _wordOrder [_index]; }
		}

		public int NextWordIndex
		{
			get { return _wordOrder [_index + 1]; }
		}

		public List<WordDefinition> Words
		{
			get { return _words; }
		}

		public GameState (List<WordDefinition> words)
		{
			_words = words;
			_wordOrder = GenerateRandomIndexes ();
		}

		private int[] GenerateRandomIndexes() {
			System.Random rnd = new System.Random();

			int[] indexes = new int[numberOfWords];
			for (int i = 0; i < indexes.Length; i++)
			{
				indexes[i] = i;
			}

			return indexes.OrderBy(x => rnd.Next()).ToArray();
		}

		public void AdvanceWord()
		{
			if (!_done && _index < numberOfWords - 1)
			{
				_index++;
			}
			else
			{
				_index = -1;
				_done = true;
			}
		}
			
	}
}

