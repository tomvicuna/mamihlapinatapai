﻿using UnityEngine;
using System.Collections;

public class AudioManager : MonoBehaviour {

	[SerializeField] AudioSource completeAudio;
	[SerializeField] AudioSource wrongAudio;
	[SerializeField] AudioSource correctAudio;

	public void PlayRightGuessAudio(){
		correctAudio.Play();
	}
	public void PlayWrongGuessAudio(){
		wrongAudio.Play();
	}
	public void PlayCompleteLevel(){
		completeAudio.Play();
	}

	void PlaySafeAudio(AudioSource aud){
		if(!aud.isPlaying){
			aud.Play();
		}
	}
}
