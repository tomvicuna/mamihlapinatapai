﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class QuestionWordJuice : MonoBehaviour {
	public bool IsJuiceOn {get; set;}

	Text questionWord;
	float fontSize;
	float amount = 5;
	// Use this for initialization
	void Awake () {
		questionWord = GetComponent<Text>();
		fontSize = questionWord.fontSize;
	}

	void OnEnable(){
		IsJuiceOn = true;
		StartCoroutine(GrowAndShrink());
	}

	void OnDisable(){
		IsJuiceOn = false;
	}

	IEnumerator GrowAndShrink(){
		while(IsJuiceOn){
			while(questionWord.fontSize < fontSize+amount){
				questionWord.fontSize +=1;
				yield return new WaitForSeconds(.05f);
			}
			while(questionWord.fontSize > fontSize-amount){
				questionWord.fontSize -=1;
				yield return new WaitForSeconds(.05f);
			}
		}
		yield return null;
	}
}
