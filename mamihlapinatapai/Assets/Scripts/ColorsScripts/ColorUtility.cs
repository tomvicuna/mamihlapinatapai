﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace Mamihlapinatapai
{
	public class ColorUtility : MonoBehaviour {
		[SerializeField] ColorPallete mapudungun;
		[SerializeField] ColorPallete aymara;
		[SerializeField] ColorPallete rapanui;


		public ColorPallete GetPallete(GameLanguage language){
			switch(language){
				case GameLanguage.Aymara:
					return aymara;
				case GameLanguage.Mapudungun:
					return mapudungun;
				case GameLanguage.Rapanui:
					return rapanui;
				default:
					return rapanui;
			}
		}
	}
}
