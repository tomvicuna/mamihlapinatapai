﻿using UnityEngine;
using System.Collections;

public class ColorPallete : MonoBehaviour {
	public Color questionColor;
	public Color boxColor;
	public Color answerColor;
	public Color guessColor;
	public Color backgroundColor;
	public Color wrongColor;
	public Color rightColor;
}
