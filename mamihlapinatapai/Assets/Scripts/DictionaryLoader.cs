﻿﻿using System;
using System.IO;
using System.Collections.Generic;
using UnityEngine;
//using Object = UnityEngine.Object;

namespace Mamihlapinatapai
{
	public class DictionaryLoader
	{
		private const string _baseDirectory = "/Resources/";
		private const string _mapudungunFilename = "mapudungun";
		private const string _aymaraFilename = "aymara";
		private const string _rapanuiFilename = "rapanui";

		public DictionaryLoader ()
		{}

		public List<WordDefinition> Load(GameLanguage language)
		{
			string filename = null;

			switch (language)
			{
			case GameLanguage.Mapudungun:
				filename = _mapudungunFilename;
				break;
			case GameLanguage.Aymara:
				filename = _aymaraFilename;
				break;
			case GameLanguage.Rapanui:
				filename = _rapanuiFilename;
				break;
			}

			return ParseTextFile (filename);
		}

		public List<WordDefinition> ParseTextFile(string path)
		{
			var dictionary = new List<WordDefinition> ();
			TextAsset ta = Resources.Load(path) as TextAsset;
			string[] lines = ta.text.Split("\n"[0]);

			foreach (string line in lines) 
			{
				char[] wordDefinitionDelimiter = { '-' };
				string[] wordDefinition = line.Split(wordDefinitionDelimiter);

				char[] wordDelimiter = { ',', ';' };
				string[] words = wordDefinition [1].Split (wordDelimiter);
				string[] definitions = wordDefinition [0].Split (wordDelimiter);

				dictionary.Add (new WordDefinition(words [0].Trim(), definitions [0].Trim()));



			}


			return dictionary;
		}


	}
}

