﻿using System;

using UnityEngine;
using UnityEngine.UI;

namespace Mamihlapinatapai
{
	public class WordDefinitionViewModel
	{
		private WordDefinition _wordDefinition;

		private Text _word;
		private Text _definition;
		private Text _guess;

		public WordDefinition WordDefinition
		{
			set
			{
				if (_wordDefinition != value)
				{
					_wordDefinition = value;
					CopyFromDomainEntity ();
				}
			}
		}

		public Text Word
		{
			get { return _word; }
		}

		public Text Definition
		{
			get { return _definition; }
		}

		public Text Guess
		{
			get { return _guess; }
		}

		public bool Guessed
		{
			get {return !Guess.enabled; }
		}

		public WordDefinitionViewModel (Definition wordDefinition)
		{
			_word = wordDefinition.word;
			_definition = wordDefinition.definition;
			_guess = wordDefinition.guess;

			HideWord ();
		}

		private void CopyFromDomainEntity()
		{
			_word.text = _wordDefinition.Word;
			_definition.text = _wordDefinition.Definition;
			_guess.text = _wordDefinition.Definition;
		}

		public void SetColor(Color answerColor, Color guessColor)
		{
			_word.color = answerColor;
			_definition.color = answerColor;
			_guess.color = guessColor;
		}

		public void ShowWord()
		{
			_word.enabled = true;
			_definition.enabled = true;
			_guess.enabled = false;
		}

		public void HideWord()
		{
			_word.enabled = false;
			_definition.enabled = false;
			_guess.enabled = true;
		}

		public void Reset()
		{
			HideWord ();
		}
	}
}

