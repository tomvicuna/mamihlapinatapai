﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScoreManager : MonoBehaviour {

	public static int PlayerScore { get; private set;}
	public static int BestScore { get; private set;}

	[SerializeField] Text playerScoreText;
	[SerializeField] Text bestScoreText;

	public void Init(){
		PlayerScore = 0;
		BestScore = PlayerPrefs.GetInt("Best", BestScore);
		RefreshUI();
	}

	public void AddScore(int amount = 2){
		PlayerScore+=amount;

		if(PlayerScore > BestScore){
			BestScore = PlayerScore;
			PlayerPrefs.SetInt("Best", BestScore);
		}

		RefreshUI();
	}

	public void SubstractScore(int amount = 1){
		if(PlayerScore > 0){
			PlayerScore-=amount;
		}
		RefreshUI();
	}



	void RefreshUI(){
		playerScoreText.text = PlayerScore.ToString();
		bestScoreText.text = BestScore.ToString();
	}
}
