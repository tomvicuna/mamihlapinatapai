﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;


namespace Mamihlapinatapai
{
	public class MainViewModel
	{
		private ColorPallete _colorPallete;
		private GameState _state;

		private Text _question;
		private List<WordDefinitionViewModel> _words = new List<WordDefinitionViewModel>();
		private Image _background;
		private Image[] _boxes;

		public ColorPallete ColorPallete
		{
			get { return _colorPallete; }
			set
			{
				if (_colorPallete != value)
				{
					_colorPallete = value;
					UpdateColors ();
				}
			}
		}

		public GameState State
		{
			get { return _state; }
			set
			{
				if (_state != value)
				{
					_state = value;
					CopyFromDomainEntity ();
				}
			}
		}

		public List<WordDefinitionViewModel> Words
		{
			get { return _words; }
		}

		public Image Background
		{
			get { return _background; }
			set { _background = value; }
		}

		public Image[] Boxes
		{
			get { return _boxes; }
			set { _boxes = value; }
		}

		public Text Question
		{
			get { return _question; }
			set { _question = value; }
		}

		public MainViewModel ()
		{
		}

		public void CopyFromDomainEntity()
		{
			Question.text = State.CurrentWord.Word;

			int count = 0;
			foreach (var word in Words) {
				word.WordDefinition = State.Words [count++];
			}

			Reset ();
		}

		public void AddWord(WordDefinitionViewModel word)
		{
			Words.Add (word);
		}
			
		public void UpdateColors()
		{
			Background.color = ColorPallete.backgroundColor;
			foreach (var box in Boxes) {
				box.color = ColorPallete.boxColor;
			}
			Question.color = ColorPallete.questionColor;

			foreach (var word in Words) {
				word.SetColor (ColorPallete.answerColor, ColorPallete.guessColor);
			}
		}

		public void WordGuessed(int index)
		{
			Words[index].ShowWord();
			Question.text = State.NextWord.Word;
		}

		public void SetBackgroundWrongColor()
		{
			Background.color = ColorPallete.wrongColor;
		}

		public void SetBackgroundRightColor()
		{
			Background.color = ColorPallete.rightColor;
		}

		public void SetBackgroundDefaultColor()
		{
			Background.color = ColorPallete.backgroundColor;
		}

		public void Reset() {
			foreach (var word in _words) {
				word.Reset ();
			}
		}
	}
}

