﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace Mamihlapinatapai
{
	public class StateGenerator
	{
		public StateGenerator ()
		{
		}

		public GameState Generate(List<WordDefinition> dictionary)
		{
			var words = SelectWords (dictionary, 4);
			return new GameState (words);
		}

		List<WordDefinition> SelectWords(List<WordDefinition> dictionary, int numberOfWords) 
		{
			System.Random rnd = new System.Random ();
			return dictionary.OrderBy (x => rnd.Next()).Take (numberOfWords).ToList();
		}
	}
}

