﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class MovingBackgroundManager : MonoBehaviour {
	public GameObject letterPrefab;
	List<RectTransform> lettersTrans = new List<RectTransform>();

	void Awake(){

		for(int i = 0; i < 30;i ++){
			for(int j = 0 ; j < 50; j++){
				GameObject temp = Instantiate(letterPrefab, new Vector3(letterPrefab.transform.position.x+j*40, letterPrefab.transform.position.y-i*40, 0f), Quaternion.identity) as GameObject;
				RectTransform recT = temp.GetComponent<RectTransform>();
				recT.SetParent(this.gameObject.GetComponent<RectTransform>());
				recT.localScale = new Vector3(1f,1f,1f);
				lettersTrans.Add(temp.GetComponent<RectTransform>());
			}
		}

	}

	void Update(){
		for(int i = 0; i < lettersTrans.Count; i++){
			lettersTrans[i].localPosition = new Vector3(lettersTrans[i].localPosition.x -5f, lettersTrans[i].localPosition.y -5, lettersTrans[i].localPosition.x);
	//		Debug.Log(lettersTrans[i].localPosition.x);
			if(lettersTrans[i].localPosition.x < -1000){
				lettersTrans[i].localPosition = new Vector3(1000, lettersTrans[i].localPosition.y, lettersTrans[i].localPosition.x);
			}
			if(lettersTrans[i].localPosition.y < -548){
				lettersTrans[i].localPosition = new Vector3(lettersTrans[i].localPosition.x, 548, lettersTrans[i].localPosition.x);
			}

		}
	}
}
