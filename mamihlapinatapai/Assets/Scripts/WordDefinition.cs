﻿using System;

namespace Mamihlapinatapai
{
	public class WordDefinition
	{
		private string _word;
		private string _definition;

		public string Word {
			get { return _word; }
		}

		public string Definition {
			get { return _definition; }
		}

		public WordDefinition (string word, string definition)
		{
			this._word = word;
			this._definition = definition;
		}

		public bool isDefinition (string definition)
		{
			return string.Equals (this._definition, definition);
		}
	}
}

